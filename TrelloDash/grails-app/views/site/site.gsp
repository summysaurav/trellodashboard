<html>
<head>
<style type="text/css">
.form-container{
	margin: 139px 0px 1px 534px;
	border: solid 1px gray;
	width: 300px;
	height: 170px;
	border-radius: 7px;
	align-items: center;
	text-align: center;
	background: #E6E6E6;
}

.trello-link{
	width: 223px;
    height: 30;
    border: solid 1px gray;
    border-radius: 4px;
}

.submit-btn{
	margin: 17px;
    width: 108px;
    background: green;
    color: white;
    height: 33px;
    border-radius: 6px;
}
.form{
    margin: 40px;
}


</style>
</head>
<body>

<div id="container" class="form-container">
<g:form controller="dashboard" method="POST" class="form">
	<input type ="text" name="trelloLink" value="" placeholder="Enter Trello Link" class="trello-link" />
	<g:submitButton class="submit-btn" name="Get Trello Board"/>
</g:form>

</div>

</body>
</html>