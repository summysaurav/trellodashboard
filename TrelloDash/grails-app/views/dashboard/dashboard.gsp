<html>
<head>
	<script  src="http://code.jquery.com/jquery-1.7.1.min.js"></script>
	<script type="text/javascript"	src="https://api.trello.com/1/client.js?key=c980e22af769f36a118aebb96a6bf0a0"></script>
	<style type="text/css">
		.card-container{
		    border: solid 1px gray;
		    display: inline-block;
		    margin: 33px 10px 4px 2px;
		    width: 25%;
		    min-height: 200px;
		    vertical-align: top;
		    box-shadow: 10px 10px 5px #888888;
		}
		.b-name{
		    border-bottom: solid 1px gray;
   			text-align: center !important;
		}
		p:not(.b-name){
		    margin: 8px 2px;
		    background: gray;
		    width: 320px;
		    height: 36px;
		    /* padding: 1px; */
		    border-radius: 3px;
		    /* box-shadow: 5px 8px 5px #8C8C8C; */
		    cursor: pointer;
		    color: white;
		    padding: 1px;
		    text-align: center;
		}
		#loadingMsg{
		    display: none; 
		    margin: 27px 601px 0px;
		    z-index: 999;
		    font-size: 19px;
		}
		#cardStatusContainer{
			width: 50%;
		    margin-top: 40px;
		    margin-left: 379px;
		    height: 10p;
		    border: solid 1px gray;
		    border-radius: 6px;
		    background: #E6E6E6;
		}
		.card-child-container{
	        margin: 1px 157px;
	    	border: solid 1px gray;
		}
		.close{
			margin: 648px;
		    border: solid 1px gray;
		    border-radius: 1px;
		    background: red;
		    color: white;
		    cursor: pointer;
		}
	</style>
</head>
<body>
	<script type="text/javascript">
		var authenticationSuccess = function() {
			console.log('Successful authentication');
		};
		var authenticationFailure = function() {
			console.log('Failed authentication');
		};

		Trello.authorize({
			type : 'popup',
			name : 'AppDr',
			scope : {
				read : true,
				write : true
			},
			expiration : 'never',
			success : authenticationSuccess,
			error : authenticationFailure
		});

		function getTrelloBoard(id) {
			var boardId = id;
			$("#bTr").html("");
			Trello.get('boards/'+boardId+'/lists',{cards:'all'},function(lists){
				var div="";
				$.each(lists,function(ix,list){
						var cards = list.cards;
						div += '<div class="card-container" id="bTd"><p class="b-name">'+list.name+'</p>'
						for(var i=0;i<cards.length;i++){
							div+='<p id="cardsId">'+cards[i].name+'<input type="hidden" value="'+cards[i].id+'"></p>'
						}
						div+='</div>'
				})
				$("#bTable").find("#bTr").append(div);
				$("#boards").slideDown();
			});
			
		}


		function getTimeCaluclation(cardId){
			$("#loadingMsg").show();
			$.ajax({
			    url: "${createLink(action:'getTimeToDoneCard')}",
				type:"POST",
				data:"cardId="+cardId,
				success: function( response) {
						var element=''
						if(response.length==0){
							element += '<div class="card-child-container">'
							element += '<p>No member has accessed this card yet.</p>'
							element += '</div>'								
						}else{
							$(response).each(function(indexItem,card){
								element += '<div class="card-child-container">'
									element +='<p> Member : '+card.memberFullName+'</p>'
								if(card.minutesTakenToDone===0 && card.hoursTakenToDone===0 && card.DayTakenToDone===0){
									element += '<p style="hei" >Status : Card is in progress</p>'
									element +=	'<p>Cards moved to in progress :'+card.cardTransitionStatus+'</p>'
								}else{
									element += '<p>Status : Card is in Done</p>'
									element += '<p>Card done at : '+ new Date(card.cardDoneStatus)+'</p>'
									element += '<p>Hours Taken to complete :'+ parseFloat(card.hoursTakenToDone).toFixed("2")+'</p>'
									element += '<p>Minutes Taken to complete :' +parseFloat(card.minutesTakenToDone).toFixed("2")+'</p>'
									element += '<p>Days Taken to complete :' +parseFloat(card.DayTakenToDone).toFixed("0")+'</p>'
									
								}
								element += '</div>'	
							})
						}	
						$("#loadingMsg").hide();
						$("#cardStatusContainer").html(element);
						$("#cardStatusContainer").slideDown();

							
				 },
				 error: function(error) { // if error occured
				        console.log("an error occurred "  + error);
				    },
				 complete: function(){
					console.log("complete")
				 }
			});
		}

		$(document).on("ready",function(){
			$(".cards").click(function(){
				var cardId = $(this).find("input").val();
						getTimeCaluclation(cardId)
			});

			$(".close").click(function(){
					$("#cardStatusContainer").hide();
				})
		})
		
	</script>
	
	
	<div id="boards" >
		<div border="1" id="bTable">
			<div id="bTr" >
			<g:each in="${trelloListInfo}" var ="trelloList">
				<div class="card-container" id="bTd">
					<p class="b-name">${trelloList?.listName}</p>
					<g:each in="${trelloList?.cards}" var="card">
						<p id="cardsId" class="cards">${card?.cardName} <input type="hidden" value="${card?.cardId}"></p>
					</g:each>
				</div>
			</g:each>
			</div>
		</div>
	
	</div>
	<div id="listContainer" align="center" style="display: none;border: 1px gray; margin: 2% 0% 0% -14%;border: 1px gray;">
	</div>
	
	<div id="loadingMsg">Loading Status...</div>
	
	<div id="cardStatusContainer" style="display: none;">
		
	</div>
</body>
</html>