package com.trello.dashboard

import grails.converters.JSON
import grails.util.Holders

import java.text.DateFormat
import java.text.SimpleDateFormat

import org.apache.commons.validator.GenericValidator
import org.codehaus.groovy.grails.commons.GrailsApplication


class DashboardController {

	def rest,timeCalculationService;
	GrailsApplication grailsApplication = Holders.getGrailsApplication();
	
	public final String trelloAppKey= grailsApplication.config.grails.trello.appKey;
	public final String trelloToken= grailsApplication.config.grails.trello.token;
	
	static defaultAction = "dashboard"
	def dashboard(){
		List trelloLists = new ArrayList();
		try{
			if(GenericValidator.isBlankOrNull(params.trelloLink)){
				render "<p>Invalid Input !!!</p></br><p>Please Go Back and try again</p>"
			}else{
				String trelloLink = params.trelloLink+".json";
				log.info "Trello link :: "+ params.trelloLink
				def resp = rest.get(trelloLink);
				if(resp!=null){
					String boardId = resp.json.id;
					def respTogetCards = rest.get("https://api.trello.com/1/boards/${boardId}/lists?key=${trelloAppKey}&token=$trelloToken&cards=all")
					if(respTogetCards!=null){
						respTogetCards.json.each{
							List listOfTrelloCards = new ArrayList();
							it.cards.each{
								if(it.size()!=0){
									def trelloCardsInfo = [cardId:it.id,cardName:it.name];
									listOfTrelloCards.add(trelloCardsInfo);
								}
							}
							def trelloListInfo = [id:it.id,listName:it.name,cards:listOfTrelloCards];
							trelloLists.add(trelloListInfo);
						}
					}
					else{
						render "No Bords and Cards available"
					}
				}
			}
			[trelloListInfo:trelloLists]
		}catch(Exception e){
			log.error "error occured while getting boards :: ${e.getMessage()}"
			render "<p>Oops</p><p>Some thing went wrong..</p><p>Check your internet connectivity</p>"
			
		}
	}
	
	def getTimeToDoneCard(){
		def cardStatusByMember = []
		try{
			if(!GenericValidator.isBlankOrNull(params.cardId)){
				String cardId = params.cardId;
				def respTogetCardsActions = rest.get("https://api.trello.com/1/cards/${cardId}/actions?key=${trelloAppKey}&token=$trelloToken&fields=all");
				if(respTogetCardsActions!=null){
					Date cardDoneTimeStamp,cardInProgressTimeStamp = null;
					List listOfCardsStatus = new ArrayList()
					DateFormat sdf =new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
					respTogetCardsActions.json.each{
						String memberFullName = it.memberCreator.has("fullName")?it.memberCreator.fullName:"Unknown"
						if(it.has("data")&& it.data.has("listAfter") && it.data.listAfter.name.equals("Done"))
							cardDoneTimeStamp = sdf.parse(it.date);
						if(it.has("data")&& it.data.has("listAfter") && (it.data.listAfter.name).toString().indexOf("Code Review")>=0)
							cardInProgressTimeStamp = sdf.parse(it.date);
						def cardInfo = ["memberName":memberFullName,"cardDoneTimeStamp":cardDoneTimeStamp,"cardInProgressTimeStamp":cardInProgressTimeStamp];
						listOfCardsStatus.add(cardInfo);
					}

					cardStatusByMember = timeCalculationService.getTimeCalculation(listOfCardsStatus);
					log.info "cardStatusByMember $cardStatusByMember"
					render cardStatusByMember as JSON
				}
				else{
					render "No Cards Found!!"
				}
			}
		}catch(Exception e){
			log.error "error occured while getting boards :: ${e.getMessage()}"
			render "<p>Oops</p><p>Some thing went wrong..</p><p>Check your internet connectivity</p>"
		}
	}
	
}
