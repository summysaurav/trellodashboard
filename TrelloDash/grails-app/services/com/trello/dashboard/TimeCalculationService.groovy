package com.trello.dashboard

import grails.transaction.Transactional

@Transactional
class TimeCalculationService {

   	
	def getTimeCalculation(List actions){
		
		try{
			Date cardDoneTime,cardInProgressTime;
			List cardStatusByMember = new ArrayList(); 
			actions.each{action->
				if(action.cardDoneTimeStamp!=null && action.cardInProgressTimeStamp!=null){
					cardDoneTime = (Date)action.cardDoneTimeStamp;
					cardInProgressTime = (Date)action.cardInProgressTimeStamp;
					def diff = cardDoneTime.getTime().minus(cardInProgressTime.getTime());
					def diffMinutes, diffHours, diffDays;
					diffMinutes = diffHours = diffDays=0;
					if(diff>0){
						 diffMinutes = diff.div(60.multiply(1000))
						 diffHours = diffMinutes.div(60)
						 diffDays = diffHours.div(24)
					}
					cardStatusByMember.add([memberFullName:action.memberName,cardTransitionStatus:cardInProgressTime,cardDoneStatus:cardDoneTime,
													minutesTakenToDone:diffMinutes,hoursTakenToDone:diffHours, DayTakenToDone:diffDays
											]);
				}
				
			}
			log.info "cards status info by member : ${cardStatusByMember}"
			cardStatusByMember
		}catch(Exception e){
			log.error "Error in Calculating time" 
			e.printStackTrace();
		}
	}
}
